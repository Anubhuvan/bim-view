import 'element-ui/lib/theme-chalk/index.css'
import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'
import ElementUI from 'element-ui'

const _1e69543d = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)
Vue.use(ElementUI)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/",
    component: _1e69543d,
    name: "index1"
  },
  {
    path: "/:viewId",
    component: _1e69543d,
    name: "index"
  }
],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}

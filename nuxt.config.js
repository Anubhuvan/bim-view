
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel:"stylesheet" ,href:"https://geoapi.youbim.com:8181/fonts/font-awesome.min.css"},
      {rel:"stylesheet", href:"https://geoapi.youbim.com:8181/css/styles.css"},
    ],
    script: [
      { type:"text/javascript" ,src: "https://static.facilio.com/apps-sdk/latest/facilio_apps_sdk.min.js"},
      { id: "YouBIMViewerImport"  , src : "https://geoapi.youbim.com:8181/app.min.js"},
    ]

  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
